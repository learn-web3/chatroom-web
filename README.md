# Chatroom (chatroom-client)

Smart contract chat room

# Application summary

Hi!

This is my first client-side application made during the education of Ethyrium smart contracts and Web3 app creation. To check its features, you need to have the Metamask browser extension installed, connect it to the Rinkeby network and have some funds on your testing account. You can retrieve free testing eth using any Rinkeby faucet recourse.
You can check the demo following by the link: http://chatroom-web3.surge.sh

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
