// This is just an example,
// so you can safely delete all default props below

export default {
  noAuth: {
    disclaimer: {
      text1:
        "Добро пожаловать! Это тестовый проект, созданный в процессе обучения созданию веб приложений на основе смарт-контрактов Ethereum. Для того, чтобы проверить его работу, вам необходимо установить приложение для браузера ",
      text2:
        ", подключить его к сети Rinkeby и иметь некоторе количество тестовых средств на текущем счете. Вы можете получить небольшое количество эфира, используя любой ",
      metamask: "Metamask",
      faucet: "faucet ресурс.",
    },
  },
  recheck: "Проверка",
  proceed: "Продолжить",
  loadMore: "Загрузить еще...",
  enterYourMessage: "Введите собщение",
  signUp: "Регистрация",
  extensionInstalled: "Расширение установлено",
  accessProvided: "Доступ открыт",
  connectedToRinkeby: "Подключено к Rinkeby",
  regAccessDialog: {
    title: "Открытие доступа",
    disclaimer:
      "Вам необходимо предоставить доступ к чтению данных в вашем счете Ethereum.",
    requestAccess: "Предоставление доступа",
  },
  ok: "Ок",
  reqExtDialog: {
    title: "Необходимо установить приложение",
    message: [
      "Пожалуйста, убедитесь что вы установили и активировали расширение",
      "Metamask",
      "для браузера",
    ],
  },
  gotIt: "Понятно",
  reqRinkebyDialog: {
    title: "Подключение к Rinkeby",
    message:
      "Пожалуйста, подключитесь к сети Rinkeby в расширении Metamask. Данный смарт-контракт работает только в этой сети.",
  },
  signUpDialog: {
    pickUpUsername: "Введите имя пользователя",
    yourUsername: "Имя пользователя",
    hint: "Пожалуйста, введите имя для чата (минимум 3 символа).",
  },
  pleaseWait: "Ожидание...",
  requiredField: "Необходимое поле",
  min3Sym: "Минимум 3 символа",
  nameTaken: "Данное имя уже занято",
};
