// This is just an example,
// so you can safely delete all default props below

export default {
  noAuth: {
    disclaimer: {
      text1:
        "Welcome! This is an educational project, based on the Ethereum smart contract. To check its features, you need to have the ",
      text2:
        " browser extension installed, connect it to the Rinkeby network and have some funds on your testing account. You can retrieve free testing ethereum using any ",
      metamask: "Metamask",
      faucet: "faucet resource.",
    },
  },
  recheck: "Recheck",
  proceed: "Proceed",
  loadMore: "Load more...",
  enterYourMessage: "Enter your message",
  signUp: "Sign up...",
  extensionInstalled: "Extension installed",
  accessProvided: "Access provided",
  connectedToRinkeby: "Connected to Rinkeby",
  regAccessDialog: {
    title: "Provide the access",
    disclaimer:
      "You need to provide the access to read the data from your Ethereum account.",
    requestAccess: "Request the access",
  },
  ok: "Ok",
  reqExtDialog: {
    title: "Extension installation is required",
    message: [
      "Please, check if you have installed and enabled the",
      "Metamask",
      "browser extension",
    ],
  },
  gotIt: "Got it",
  reqRinkebyDialog: {
    title: "Switch to Rinkeby network",
    message:
      "Inside you Metamask browser extension, switch the network to Rinkeby. Chatroom contract is working only inside this network.",
  },
  signUpDialog: {
    pickUpUsername: "Pick up your username",
    yourUsername: "Your username",
    hint: "Please, set up your username that the other participants will see in the chat. Your username can't be changed. Miminum 3 symbols are acceptable.",
  },
  pleaseWait: "Please, wait...",
  requiredField: "Required field",
  min3Sym: "Min 3 symbols",
  nameTaken: "This name is already taken",
};
