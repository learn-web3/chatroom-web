import { boot } from "quasar/wrappers";
import Web3 from "web3";

const address = "0x2D6bd3138D311430F25C4f1e9b055A564C973221";
const abi = [
  {
    inputs: [{ internalType: "int256", name: "cursor", type: "int256" }],
    name: "getMessages",
    outputs: [
      {
        components: [
          { internalType: "string", name: "name", type: "string" },
          { internalType: "string", name: "text", type: "string" },
          {
            internalType: "uint256",
            name: "timestamp",
            type: "uint256",
          },
          { internalType: "uint256", name: "number", type: "uint256" },
        ],
        internalType: "struct Chatroom.Message[]",
        name: "",
        type: "tuple[]",
      },
      { internalType: "uint256", name: "", type: "uint256" },
    ],
    stateMutability: "view",
    type: "function",
    constant: true,
    payable: undefined,
    signature: "0x78488bae",
  },
  {
    inputs: [],
    name: "getUser",
    outputs: [
      {
        components: [
          { internalType: "string", name: "name", type: "string" },
          { internalType: "bool", name: "registered", type: "bool" },
        ],
        internalType: "struct Chatroom.Profile",
        name: "",
        type: "tuple",
      },
    ],
    stateMutability: "view",
    type: "function",
    constant: true,
    payable: undefined,
    signature: "0x832880e7",
  },
  {
    inputs: [],
    name: "namesList",
    outputs: [{ internalType: "string[]", name: "", type: "string[]" }],
    stateMutability: "view",
    type: "function",
    constant: true,
    payable: undefined,
    signature: "0x765e81e5",
  },
  {
    inputs: [{ internalType: "string", name: "_text", type: "string" }],
    name: "send",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
    constant: undefined,
    payable: undefined,
    signature: "0x66792ba1",
  },
  {
    inputs: [{ internalType: "string", name: "_nickname", type: "string" }],
    name: "signup",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
    constant: undefined,
    payable: undefined,
    signature: "0x519c6377",
  },
];

const web3 = new Web3(window.ethereum);

const contract = new web3.eth.Contract(abi, address);

export default async ({ app }) => {
  app.config.globalProperties.$web3 = web3;
  app.config.globalProperties.$chatroom = contract;
};

export { web3, contract };
