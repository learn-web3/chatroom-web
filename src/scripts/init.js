import { reactive } from "vue";
import Web3 from "web3";
import { address, abi } from "./contract";
import { SessionStorage } from "quasar";
import { i18n } from "boot/i18n";
const gas = "3000000";

export const state = reactive({
  isExtensionActive: false,
  isAccessProvided: false,
  isRinkeby: false,
  initialized: false,
  chatroom: null,
  account: null,
  username: null,
  balance: null,
  messages: [],
  newCursor: -1,
  names: [],
});

export async function init() {
  state.loading = true;
  state.isExtensionActive = !!window.ethereum;
  if (!state.isExtensionActive) return endLoading();
  const web3 = new Web3(window.ethereum);
  const chatroom = new web3.eth.Contract(abi, address);
  state.chatroom = chatroom;
  const networkId = await web3.eth.net.getNetworkType();
  let accounts;
  try {
    accounts = await web3.eth.requestAccounts();
  } catch (e) {
    console.log(e);
  }
  if (!accounts || !accounts[0]) return endLoading();
  const account = accounts[0];
  state.isAccessProvided = true;
  state.account = account;
  state.isRinkeby = networkId === "rinkeby";
  if (!state.isRinkeby) return endLoading();
  let balance = await web3.eth.getBalance(account);
  balance = web3.utils.fromWei(balance, "ether");
  state.balance = Math.round(balance * 10000) / 10000;
  const username = await chatroom.methods.getUser().call({ from: account });
  if (username && username.name) state.username = username.name;
  getMessages();
  getNames();
  state.initialized = true;
}

function endLoading() {
  state.loading = false;
  return;
}

export async function getMessages() {
  state.loading = true;
  if (!state.chatroom) await init();
  const messages = await state.chatroom.methods.getMessages(-1).call();
  appendMessages(messages[0]);
  state.newCursor = messages[1];
  endLoading();
  // console.log(messages);
  return messages;
}

export async function sendMessage(text) {
  if (!state.chatroom) await init();
  if (!state.username) return;
  await state.chatroom.methods.send(text).send({ from: state.account, gas });
  await getMessages();
}

export async function getMoreMessages() {
  state.loading = true;
  if (!state.chatroom) await init();
  const messages = await state.chatroom.methods
    .getMessages(state.newCursor)
    .call();
  appendMessages(messages[0]);
  state.newCursor = messages[1];
  console.log(state.newCursor);
  endLoading();
}

export async function getNames() {
  state.names = await state.chatroom.methods.namesList().call();
}

function appendMessages(messages) {
  const fetched = messages
    .map((m) => {
      return {
        name: m.name,
        number: m.number,
        text: m.text,
        timestamp: m.timestamp,
        time: new Intl.DateTimeFormat("ru", {
          dateStyle: "short",
          timeStyle: "short",
        }).format(new Date(m.timestamp * 1000)),
      };
    })
    .filter((m) => {
      const found = state.messages.find((st) => st.number === m.number);
      return found === undefined;
    });
  state.messages.push(...fetched);
  state.messages.sort((a, b) => a.timestamp - b.timestamp);
}

export function setSavedLang() {
  const savedLang = SessionStorage.getItem("locale");
  console.log("savedLang", savedLang);
  const locale = savedLang || "en-US";
  setLang(locale);
  return locale;
}

export function setLang(locale) {
  console.log("locale", locale);
  SessionStorage.set("locale", locale);
  i18n.global.locale.value = locale;
}

const timer = setInterval(() => {
  if (!state.isAccessProvided || !state.isRinkeby) return;
  init();
}, 5000);
